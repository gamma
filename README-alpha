This file is part of Gamma package.
See end of file for copying conditions.

* Introduction

This is a *pre-release* version, and not ready for production use yet.
If you are taking source from GIT, you will need to have several
special tools to help contribute. See the file README-hacking for more
information, See chapter `Bootstrapping' for the detailed instructions on
how to build the package.

Please, note that the accompanying documentation may be inaccurate
or incomplete. The Git logs are the authoritative documentation of
all recent changes. To convert them to the traditional ChangeLog
format, run `make ChangeLog'.

Please send comments and problem reports to <gray+gamma@gnu.org.ua>.

* Checking Out the Sources

The following instructions apply if you wish to obtain sources from
the GIT repository:

To clone the Gamma repository, issue the following command:

  git clone git://git.gnu.org.ua/gamma.git

or

  git clone http://git.gnu.org.ua/repo/gamma.git

This will create a directory named `gamma' and populate it with the
sources.

If you are not interested in the entire development history, you can
abridge it by giving the `--depth' option to clone.  For example,

  git clone --depth 2 git://git.gnu.org.ua/repo/gamma.git

will clone only two recent revisions from the repository.

For more information about GIT access, visit
http://puszcza.gnu.org.ua/git/?group=gamma

The cloned version must be bootstrapped before building. To do so,
change to the gamma source tree and run the following command:

  scripts/bootstrap --moddir modules --parents

* Building

The usual procedures apply:

 ./configure
 make
 make install

See INSTALL and README for the detailed instructions.

* Copyright information:

Copyright (C) 2002-2018 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:

