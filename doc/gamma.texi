\input texinfo @c -*-texinfo-*-
@smallbook
@c %**start of header
@setfilename gamma.info
@settitle Gamma
@c %**end of header
@setchapternewpage odd

@include version.texi
@include macros.texi
@include rendition.texi

@defcodeindex op
@defcodeindex kw
@defcodeindex fl
@syncodeindex fn cp
@syncodeindex pg cp
@syncodeindex op cp
@syncodeindex kw cp
@syncodeindex fl cp

@ifinfo
@dircategory Guile modules
@direntry
* gamma: (gamma).           Assorted Guile Modules.
@end direntry
@end ifinfo

@copying
Copyright @copyright{} 2010 Sergey Poznyakoff

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with the Front-Cover texts being ``Gamma Manual'',
and with the Back-Cover Texts as in (a) below.  A copy of the license
is included in the section entitled ``GNU Free Documentation License''.

(a) The FSF's Back-Cover Text is: ``You have freedom to copy and modify
this Manual, like GNU software. Help the software be free.''
@end copying

@titlepage
@title Gamma
@subtitle version @value{VERSION}, @value{UPDATED}
@author Sergey Poznyakoff.
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@ifnothtml
@page
@summarycontents
@page
@end ifnothtml
@contents

@ifnottex
@node Top
@top Gamma

This edition of the @cite{Gamma Manual}, last updated @value{UPDATED},
documents @GAMMA{} Version @value{VERSION}.
@end ifnottex

@menu
* Overview::
* Syslog::                  Syslog Interface.
* SQL::                     SQL Interface.
* Expat::                   Expat Interface.
* Reporting Bugs::          How to Report a Bug.

Appendices

* Copying This Manual::  The GNU Free Documentation License.
* Concept Index::        Index of Concepts.

@detailmenu
 --- The Detailed Node Listing ---

Expat Interface

* expat basics::
* creating parsers::
* parsing::
* errors::
* handlers::
* miscellaneous functions::

Expat Handlers

* start-element-handler::
* end-element-handler::
* character-data-handler::
* processing-instruction-handler::
* comment-handler::
* start-cdata-section-handler::
* end-cdata-section-handler::
* default-handler::
* default-handler-expand::
* skipped-entity-handler::
* start-namespace-decl-handler::
* end-namespace-decl-handler::
* xml-decl-handler::
* start-doctype-decl-handler::
* end-doctype-decl-handler::
* attlist-decl-handler::
* entity-decl-handler::
* notation-decl-handler::
* not-standalone-handler::

@end detailmenu
@end menu

@node Overview
@chapter Overview

@GAMMA{} is a collection of assorted Guile modules.  Version
@value{VERSION} provides a @samp{syslog} interface, a module for
interfacing with @acronym{SQL} (more precisely: MySQL and PostgreSQL)
databases and a module for writing @acronym{XML} parsers,

@node Syslog
@chapter Syslog Interface
@include syslog.texi

@node SQL
@chapter SQL Interface
@include sql.texi

@node Expat
@chapter Expat Interface
@include expat.texi

@node Reporting Bugs
@chapter How to Report a Bug

  If you think you've found a bug, please report it to
@email{gray+gamma@@gnu.org.ua}.  Be sure to include maximum
information needed to reliably reproduce it, or at least to analyze
it.  The information needed is:

@itemize
@item Version of the package you are using.
@item Compilation options used when configuring the package.
@item Run-time configuration.
@item Conditions under which the bug appears.
@end itemize

@node Copying This Manual
@appendix GNU Free Documentation License
@include fdl.texi

@node Concept Index
@unnumbered Concept Index

This is a general index of all issues discussed in this manual

@printindex cp

@bye
