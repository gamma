(use-modules (gamma expat))

(display (xml-expat-version-string))
(newline)

(let ((vinfo (xml-expat-version)))
  (format #t "Major:~A~%Minor:~A~%Micro:~A~%"
	  (list-ref vinfo 0)
	  (list-ref vinfo 1)
	  (list-ref vinfo 2)))

      