# The SQL module provides interfaces for MySQL and PostgreSQL database
# management systems.

configure:
MYSQL=yes
PGSQL=yes

AC_ARG_WITH([mysql],
            AC_HELP_STRING([--without-mysql],
                           [configure to work without MySQL]),
            [MYSQL=$withval])
AC_ARG_WITH([postgres],
            AC_HELP_STRING([--without-postgres],
                           [configure to work without Postgres]),
            [PGSQL=$withval])

AC_SUBST([SQLLIBS])
AC_SUBST([SQLOBJS])
# Check individual libraries
if test $MYSQL = yes; then
  MU_CHECK_LIB([mysqlclient], [mysql_real_connect], [-lm],
               [ AC_DEFINE([USE_SQL_MYSQL],1,
                           [Define this if you are going to use MySQL])
                 AC_DEFINE([HAVE_LIBMYSQL],1,
                           [Define this if you have mysqlclient library])
	         SQLOBJS="$SQLOBJS mysql$U.lo"
                 SQLLIBS="$SQLLIBS $mu_cv_lib_mysqlclient" ],
               [ MYSQL=no ],
               [/usr/local/lib/mysql /usr/lib/mysql])
fi
if test $PGSQL = yes; then
  MU_CHECK_LIB([pq], [PQconnectStart], [],
               [ AC_DEFINE([USE_SQL_PGSQL],1,
                           [Define this if you are going to use PostgreSQL])
                 AC_DEFINE([HAVE_LIBPQ],1,
                           [Define this if you have libp])
	         SQLOBJS="$SQLOBJS pgsql$U.lo"
                 SQLLIBS="$SQLLIBS $mu_cv_lib_pq" ],
               [ PGSQL=no ],
               [/usr/local/pgsql/lib /usr/pgsql/lib])
fi

if test ${MYSQL}${PGSQL} = nono; then
  gamma_sql=no
fi

libraries:
sql

sources:
gsql_conn.c
gsql_lib.c
guile-sql.h
app.h

makefile:
EXTRA_DIST += mysql.c pgsql.c
libgamma_sql_la_LIBADD = @SQLOBJS@ @LTLIBOBJS@ @GUILE_LIBS@ @SQLLIBS@
libgamma_sql_la_DEPENDENCIES = @SQLOBJS@

scm:
sql.scm
