/* This file is part of Gamma.
   Copyright (C) 2002, 2010 Sergey Poznyakoff

   Gamma is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Gamma is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Gamma.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <guile-sql.h>
#include <app.h>
#include <string.h>

static struct gamma_parmdcl *
find_parmdcl(struct gamma_parmdcl *dcl, const char *name)
{
	for (; dcl->name; dcl++)
		if (strcmp(dcl->name, name) == 0)
			return dcl;
	return 0;
}

void
gamma_parmlist_parse(SCM parmlist, struct gamma_parmdcl *dcltab,
		     int flags, const char *func_name)
{
	SCM elt;
	
	for (elt = parmlist; elt != SCM_EOL; elt = SCM_CDR(elt)) {
		SCM pair = SCM_CAR(elt);
		SCM kw;
		char *str;
		struct gamma_parmdcl *p;
		
		SCM_ASSERT(scm_is_pair(pair), pair, SCM_ARG1, func_name);
		
		kw = SCM_CAR(pair);
		SCM_ASSERT(scm_is_keyword(kw), kw, SCM_ARG1, func_name);

		str = scm_to_locale_string
			(scm_symbol_to_string(scm_keyword_to_symbol(kw)));

		p = find_parmdcl(dcltab, str);
		free(str);
		if (!p) {
			if (flags & GAMMA_PARMLIST_IGNORE_UNKNOWN)
				continue;
			if (flags & GAMMA_PARMLIST_WARN_UNKNOWN) {
				scm_simple_format
					(scm_current_error_port (),
					 scm_from_locale_string("~S: undefined keyword: ~S~%"),
					 scm_list_2(scm_from_locale_string(func_name),
						    kw));
				continue;
			}
			scm_misc_error(func_name,
				       "Unknown keyword: ~S",
				       scm_list_1(kw));
		}
		if (p->cvt)
			p->cvt(SCM_CDR(pair), p->valptr, func_name);
	}		
}

void
gamma_cvt_string(SCM inval, void *outval, const char *func_name)
{
	SCM_ASSERT(scm_is_string(inval), inval, SCM_ARGn, func_name);
	*(char**)outval = scm_to_locale_string(inval);
}

void
gamma_cvt_int(SCM inval, void *outval, const char *func_name)
{
	SCM_ASSERT(scm_is_number(inval), inval, SCM_ARGn, func_name);
	*(int*)outval = scm_to_int(inval);
}

extern void
sql_init()
{
	gsql_conn_init();
#ifdef USE_SQL_MYSQL
	sql_register_iface(&mysql_iface);
#endif
#ifdef USE_SQL_PGSQL
	sql_register_iface(&pgsql_iface);
#endif
}


