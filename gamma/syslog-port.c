/* This file is part of Gamma.
   Copyright (C) 2002-2018 Sergey Poznyakoff

   Gamma is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Gamma is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Gamma.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <libguile.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

#ifndef HAVE_SCM_T_OFF
typedef off_t scm_t_off;
#endif
 
#define GAMMA_SYSLOG_PORT_BUFSIZE 1024
static scm_t_port_type *scm_syslog_port_type;
struct _gamma_syslog_port {
	int prio;
};

static SCM
_make_syslog_port(int prio)
{
	struct _gamma_syslog_port *dp;

	dp = scm_gc_typed_calloc (struct _gamma_syslog_port);
	dp->prio = prio;
	return scm_c_make_port(scm_syslog_port_type,
			       SCM_OPN | SCM_WRTNG | SCM_BUFLINE,
			       (scm_t_bits) dp);
}

#define SYSLOG_PORT(x) ((struct _gamma_syslog_port *) SCM_STREAM (x))

static void
_syslog_port_close(SCM port)
{
	struct _gamma_syslog_port *dp = SYSLOG_PORT(port);
	/* nothing */
}

static size_t
_syslog_port_write(SCM port, SCM src, size_t start, size_t count)
{
	struct _gamma_syslog_port *dp = SYSLOG_PORT(port);
	syslog(dp->prio, "%*.*s",
	       (int) count,
	       (int) count,
	       SCM_BYTEVECTOR_CONTENTS(src) + start);
	return count;
}

static scm_t_off
_syslog_port_seek(SCM port, scm_t_off offset, int whence)
{
	return (scm_t_off) -1;
}

static int
_syslog_port_print(SCM exp, SCM port, scm_print_state *pstate)
{
	scm_puts("#<", port);
	scm_print_port_mode(exp, port);
	scm_puts("Gamma syslog port>", port);
	return 1;
}

SCM_DEFINE_PUBLIC(scm_open_syslog_port, "open-syslog-port", 1, 0, 0,
		  (SCM prio),
"Create and open an output port connected to syslog priority "
"@code{prio}.")
#define FUNC_NAME s_scm_open_syslog_port
{
	SCM_ASSERT(scm_is_integer(prio), prio, SCM_ARG1, FUNC_NAME);
	return _make_syslog_port(scm_to_int(prio));
}
#undef FUNC_NAME	   

void
_gamma_init_syslog_port()
{
	scm_syslog_port_type = scm_make_port_type("syslog-port",
						  NULL,
						  _syslog_port_write);
	scm_set_port_print(scm_syslog_port_type, _syslog_port_print);
	scm_set_port_close(scm_syslog_port_type, _syslog_port_close);
	scm_set_port_seek(scm_syslog_port_type, _syslog_port_seek);
#include <syslog-port.x>	
}

