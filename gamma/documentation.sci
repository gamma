;;;; This file is part of Gamma.                           -*- scheme -*-
;;;; Copyright (C) 2002, 2008, 2010 Sergey Poznyakoff
;;;; 
;;;; Gamma is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3, or (at your option)
;;;; any later version.
;;;;
;;;; Gamma is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with Gamma.  If not, see <http://www.gnu.org/licenses/>.

changequote([,])dnl

(define-module (gamma documentation)
  :use-module (ice-9 documentation))
  
(let ((docfile "SITEDIR/guile-procedures.txt"))
  (if (not (member docfile documentation-files))
      (set! documentation-files (append documentation-files 
					(list docfile)))))

  
;;;; End of documentation.scm
