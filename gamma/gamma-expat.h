/* This file is part of Gamma.
   Copyright (C) 2010 Sergey Poznyakoff

   Gamma is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Gamma is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Gamma.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <setjmp.h>
#include <string.h>
#include <stdlib.h>
#include <libguile.h>
#include <expat.h>

#define GAMMA_EXPORT(s) s

enum gamma_expat_handler {
	start_element_handler,
	end_element_handler,
	character_data_handler,
	processing_instruction_handler,
	comment_handler,
	start_cdata_section_handler,
	end_cdata_section_handler,
	default_handler,
	default_handler_expand,
	external_entity_ref_handler,
	skipped_entity_handler,
	unknown_encoding_handler,
	start_namespace_decl_handler,
	end_namespace_decl_handler,
	xml_decl_handler,
	start_doctype_decl_handler,
	end_doctype_decl_handler,
	element_decl_handler,
	attlist_decl_handler,
	entity_decl_handler,
	unparsed_entity_decl_handler,/* Deprecated */
	notation_decl_handler,
	not_standalone_handler,
	
	gamma_expat_handler_count	
};	

struct gamma_expat_user_data {
	SCM handler[gamma_expat_handler_count];
	SCM external_entity_ref_handler_arg;
};

SCM gamma_eval_catch_body(void *list);
int gamma_safe_exec(SCM (*handler) (void *data), void *data, SCM *result);
char *gamma_proc_name(SCM proc);
