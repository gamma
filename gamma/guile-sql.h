/* This file is part of Gamma.
   Copyright (C) 2002, 2007, 2010 Sergey Poznyakoff

   Gamma is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Gamma is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Gamma.  If not, see <http://www.gnu.org/licenses/>. */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <libguile.h>

#define MAX_IFACES 32

struct sql_connect {
	int iface;
	char *hostname;
	struct in_addr hostaddr;
	int port;
	char *username;
	char *database;
	void *data;
};

struct sql_result {
	SCM owner;
	void *data;
};

struct sql_iface {
	char *name;
	SCM (*mark) (struct sql_connect *);
	size_t (*free) (struct sql_connect *);
	SCM (*connect) (SCM parmlist, const char *func_name);
	void (*close) (struct sql_connect *);
	SCM (*query) (struct sql_connect *, const char *query);
};

extern SCM gamma_sql_error;

struct gamma_parmdcl {
	const char *name;
	void *valptr;
	void (*cvt) (SCM inval, void *outval, const char *func_name);
};

#define GAMMA_PARMLIST_IGNORE_UNKNOWN 0x0001
#define GAMMA_PARMLIST_WARN_UNKNOWN   0x0002

SCM sql_connect_create(char *name);
	
void gamma_parmlist_parse(SCM parmlist,
			  struct gamma_parmdcl *dcltab, int flags,
			  const char *func_name);
void gamma_cvt_string(SCM inval, void *outval, const char *func_name);
void gamma_cvt_int(SCM inval, void *outval, const char *func_name);

void gsql_conn_init(void);
int sql_register_iface(struct sql_iface *ifp);
