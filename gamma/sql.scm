;;;; This file is part of Gamma.                           -*- scheme -*-
;;;; Copyright (C) 2002-2018 Sergey Poznyakoff
;;;; 
;;;; Gamma is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3, or (at your option)
;;;; any later version.
;;;;
;;;; Gamma is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with Gamma.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gamma sql)
  :use-module (gamma documentation)
  :use-module (gamma loader)
  :export-syntax (sql-catch-failure sql-ignore-failure))

(gamma-module-load "sql" "sql_init")

(define-syntax sql-catch-failure
  (syntax-rules ()
    ((sql-catch-failure (handler) expr)
     (catch 'sql-error
	    (lambda () expr)
	    (lambda args
	      (apply handler args))))
    ((sql-catch-failure expr)
     (sql-catch-failure (sql-error-handler) expr))))
			

(define-syntax sql-ignore-failure
  (syntax-rules ()
    ((sql-ignore-failure (value) expr)
     (catch 'sql-error
	    (lambda () expr)
	    (lambda args
	      value)))
    ((sql-ignore-failure expr)
     (sql-ignore-failure (#f) expr))))

;;;; For compatibility with v. 1.1
(define-public (sql-connect iface host port db user pass)
  (sql-open-connection (list
			(cons #:iface iface)
			(cons #:host host)
			(cons #:port port)
			(cons #:db db)
			(cons #:user user)
			(cons #:pass pass))))

(define-public (sql-connect-close conn)
  (sql-close-connection conn))

;;;; End of sql.scm
