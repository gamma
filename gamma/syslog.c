/* This file is part of Gamma.
   Copyright (C) 2002, 2010 Sergey Poznyakoff

   Gamma is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Gamma is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Gamma.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <libguile.h>
#include <syslog.h>

static char *log_tag;

SCM_DEFINE_PUBLIC(scm_openlog, "openlog", 3, 0, 0,
	  (SCM ident, SCM option, SCM facility),
"Opens a connection to the system logger for Guile program.\n\
The arguments @var{ident}, @var{option} and @var{facility} have the same \
meaning as in openlog(3)\n")
#define FUNC_NAME s_scm_openlog
{
	SCM_ASSERT(scm_is_string(ident), ident, SCM_ARG1, FUNC_NAME);
	if (log_tag)
		free(log_tag);
	log_tag = scm_to_locale_string(ident);
	SCM_ASSERT(scm_is_integer(option), option, SCM_ARG2, FUNC_NAME);
	SCM_ASSERT(scm_is_integer(facility), facility, SCM_ARG3, FUNC_NAME);
	openlog(log_tag, scm_to_int(option), scm_to_int(facility));
	return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE_PUBLIC(scm_openlog_p, "openlog?", 0, 0, 0,
		  (),
"Returns @samp{#t} if @code{openlog} has already been called.\n")
#define FUNC_NAME s_scm_openlog_p
{
	return log_tag ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE_PUBLIC(scm_syslog_tag, "syslog-tag", 0, 0, 0,
		  (),
"Returns the tag given to the recent @code{openlog} invocation.\n")
#define FUNC_NAME s_scm_syslog_tag
{
	if (!log_tag)
		scm_misc_error("syslog-tag",
			       "openlog have not been called",
			       SCM_EOL);
	return scm_from_locale_string(log_tag);
}
#undef FUNC_NAME

SCM_DEFINE_PUBLIC(scm_syslog, "syslog", 2, 0, 0,
		  (SCM prio, SCM text),
"Distributes @var{text} via syslogd priority @var{prio}.\n")
#define FUNC_NAME s_scm_syslog
{
	int nprio;
	char *str;

	SCM_ASSERT(scm_is_integer(prio), prio, SCM_ARG1, FUNC_NAME);
	nprio = scm_to_int(prio);
  
	SCM_ASSERT(scm_is_string(text), text, SCM_ARG2, FUNC_NAME);
	str = scm_to_locale_string(text);
	syslog(nprio, "%s", str);
	free(str);
	return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE_PUBLIC(scm_closelog, "closelog", 0, 0, 0,
		  (),
"Closes the channel to the system logger opened by @code{openlog}.\n")
#define FUNC_NAME s_scm_closelog
{
	closelog();
	if (log_tag) {
		free(log_tag);
		log_tag = NULL;
	}
	return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


static struct {
	int facility;
	char *name;
} syslog_kw[] = {
#define GAMMA_CONST(s) { s, #s }
	GAMMA_CONST(LOG_AUTH),
#ifdef LOG_AUTHPRIV
	GAMMA_CONST(LOG_AUTHPRIV),
#endif
	GAMMA_CONST(LOG_CRON),
	GAMMA_CONST(LOG_DAEMON),
#ifdef LOG_FTP
	GAMMA_CONST(LOG_FTP),
#endif
	GAMMA_CONST(LOG_USER),
	GAMMA_CONST(LOG_LPR),
	GAMMA_CONST(LOG_MAIL),
	GAMMA_CONST(LOG_NEWS),
	GAMMA_CONST(LOG_SYSLOG),
	GAMMA_CONST(LOG_UUCP),
	GAMMA_CONST(LOG_LOCAL0),
	GAMMA_CONST(LOG_LOCAL1),
	GAMMA_CONST(LOG_LOCAL2),
	GAMMA_CONST(LOG_LOCAL3),
	GAMMA_CONST(LOG_LOCAL4),
	GAMMA_CONST(LOG_LOCAL5),
	GAMMA_CONST(LOG_LOCAL6),
	GAMMA_CONST(LOG_LOCAL7),
  /* severity */
	GAMMA_CONST(LOG_EMERG),    
	GAMMA_CONST(LOG_ALERT),   
	GAMMA_CONST(LOG_CRIT),    
	GAMMA_CONST(LOG_ERR),     
	GAMMA_CONST(LOG_WARNING), 
	GAMMA_CONST(LOG_NOTICE),  
	GAMMA_CONST(LOG_INFO),    
	GAMMA_CONST(LOG_DEBUG),   
  /* options */
	GAMMA_CONST(LOG_CONS),   
	GAMMA_CONST(LOG_NDELAY), 
	GAMMA_CONST(LOG_PID),
	GAMMA_CONST(LOG_NOWAIT),
#ifdef LOG_ODELAY
	GAMMA_CONST(LOG_ODELAY),
#endif
#ifdef LOG_PERROR
	GAMMA_CONST(LOG_PERROR)
#endif
};

void
syslog_init()
{
	int i;
	extern void _gamma_init_syslog_port();
  
	for (i = 0; i < sizeof (syslog_kw)/sizeof(syslog_kw[0]); i++) {
		scm_c_define (syslog_kw[i].name,
			      scm_from_int(syslog_kw[i].facility));
		scm_c_export (syslog_kw[i].name, NULL);
	}
	_gamma_init_syslog_port();
#include <syslog.x>
}
